﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiMasterDetailCountryState.Models;

namespace WebApiMasterDetailCountryState.Controllers
{
    [Route("api/Country")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        public readonly ApplicationDbContext context;

        public CountryController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Country> Get()
        {
            return context.Countries.ToList();
        }

        [HttpGet("{id}", Name = "createdCountry")]
        public IActionResult GetById(int id)
        {
            var country = context.Countries.Include(c => c.States).FirstOrDefault(c => c.Id == id);

            if (country == null)
            {
                return NotFound();
            }

            return Ok(country);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Country country)
        {
            if (ModelState.IsValid)
            {
                context.Countries.Add(country);
                context.SaveChanges();
                return new CreatedAtRouteResult("createdCountry", new { id = country.Id }, country);
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Country country, int id)
        {
            if (country.Id != id)
            {
                return BadRequest();
            }

            context.Entry(country).State = EntityState.Modified;
            context.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var country = context.Countries.FirstOrDefault(p => p.Id == id);

            if (country == null)
            {
                return NotFound();
            }

            context.Countries.Remove(country);
            context.SaveChanges();
            return Ok(country);
        }
    }
}