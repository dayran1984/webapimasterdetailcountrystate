﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiMasterDetailCountryState.Models;

namespace WebApiMasterDetailCountryState.Controllers
{
    [Route("api/Country/{CountryId}/State")]
    [ApiController]
    public class StateController : ControllerBase
    {
        public readonly ApplicationDbContext context;

        public StateController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<State> GetAll(int CountryId)
        {
            return this.context.States.Where(p => p.CountryId == CountryId).ToList();
        }

        [HttpGet("{id}", Name = "stateById")]
        public IActionResult GetById(int id, int CountryId)
        {
            var state = this.context.States.FirstOrDefault(p => p.Id == id && p.CountryId == CountryId);

            if (state == null)
            {
                return NotFound();
            }

            return new ObjectResult(state);
        }

        [HttpPost]
        public IActionResult Create([FromBody] State state, int CountryId)
        {
            state.CountryId = CountryId;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            context.States.Add(state);
            context.SaveChanges();

            return new CreatedAtRouteResult("stateById", new { id = state.Id }, state);
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromBody] State state, int id)
        {
            if (state.Id != id)
            {
                return BadRequest();
            }

            context.Entry(state).State = EntityState.Modified;
            context.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var state = this.context.States.FirstOrDefault(p => p.Id == id);

            if (state == null)
            {
                return NotFound();
            }

            context.States.Remove(state);
            context.SaveChanges();
            return Ok(state);
        }
    }
}