﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiMasterDetailCountryState.Models
{
    public class Country
    {
        public Country()
        {
            States = new List<State>();
        }

        public int Id { get; set; }

        [StringLength(30)]
        public string Name { get; set; }

        public List<State> States { get; set; }
    }
}
